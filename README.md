# my_task



## Exercise 1

Create a repo on Gitlab
Set it up on your system
Set up these git aliases (https://www.freecodecamp.org/news/how-to-use-git-aliases/) to improve your git productivity (Alternative tutorial here (https://snyk.io/blog/10-git-aliases-for-faster-and-productive-git-workflow/))

## Exercise 2
Create and add your SSH public key (https://docs.gitlab.com/ee/ssh/index.html), for enabling Git over SSH.
Create a merge request (https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html), to request changes made in a branch be merged into a project’s repository.
Setup 2FA (https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html) on your Gitlab Account




